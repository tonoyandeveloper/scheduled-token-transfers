pragma solidity 0.4.24;

import "./ScheduleClient.sol";
import "./IScheduleClient.sol";

contract Controller {
    address private _owner;

    uint256 private constant _gasPrice = 16000000000; // gas price is 16 gwei by default
    uint256 private constant _gasLimit = 140000;      // gas limit is 140,000 by default

    mapping(address => address) private _clientAccount; // User address - > Contract address

    event ScheduleCallEvent(address owner, address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay);

    // -----------------------------------------
    // CONSTRUCTOR
    // -----------------------------------------

    constructor () public {
        _owner = msg.sender;
    }

    // -----------------------------------------
    // EXTERNAL
    // -----------------------------------------

    function scheduleCall(address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay) public payable returns (address) {
        _preValidation(receiver, delay, executionsAmount, tokensPerDelay);
        _createAccount();
        IScheduleClient(_clientAccount[msg.sender]).addScheduleCall.value(msg.value)(receiver, delay, executionsAmount, tokensPerDelay);
        emit ScheduleCallEvent(msg.sender, receiver, delay, executionsAmount, tokensPerDelay);
        return _clientAccount[msg.sender];
    }

    function getUserContractAddress(address user) external view returns (address) {
        return _clientAccount[user];
    }

    // -----------------------------------------
    // INTERNAL
    // -----------------------------------------

    function _createAccount() internal {
        if (_clientAccount[msg.sender] == address(0)) {
            ScheduleClient newContract = new ScheduleClient(msg.sender);
            _clientAccount[msg.sender] = address(newContract);
        }
    }

    function _preValidation(address receiver, uint256 delay, uint256 executionsAmount, uint256 tokensPerDelay) internal view {
        require(msg.value == (_gasLimit * _gasPrice * executionsAmount), "_preValidation: msg.value is not enough for making schedule calls");
        require(receiver != address(0), "_preValidation: the receiver address is not valid");
        require(delay > 0, "_preValidation: the delay is not bigger than 0");
        require(executionsAmount > 0, "_preValidation: the executions amount is not bigger than 0");
        require(tokensPerDelay > 0, "_preValidation: tokens per delay is not bigger than 0");
    }
}
