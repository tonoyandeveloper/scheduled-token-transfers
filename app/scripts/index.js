// Import the page's CSS. Webpack will know what to do with it.
import 'normalize.css'
import 'semantic-ui-css/semantic.min.css'
import '../styles/app.css'

import { default as Web3 } from 'web3'

import controllerArtifact from '../../build/contracts/Controller.json'
import scheduleClientArtifact from '../../build/contracts/ScheduleClient.json'
import ierc20Articat from '../../build/contracts/IERC20.json'

const $ = window.jQuery

// ABI
const Controller = web3.eth.contract(controllerArtifact.abi)
const ScheduleClient = web3.eth.contract(scheduleClientArtifact.abi)
const IERC20 = web3.eth.contract(ierc20Articat.abi)

// variables
let etherscanURL
const gasLimit = 140000
const gasPrice = 16000000000

const mainnet = {
  name: 'Mainnet',
  id: '1',
  url: 'https://etherscan.io',
  controller: '',
  token: ''
}

const ropsten = {
  name: 'Ropsten',
  id: '3',
  url: 'https://ropsten.etherscan.io',
  controller: '0x92ae3b4bfa26df0ed7c09bf103443b8a4d897e7a',
  token: '0x2b348aAe30E1183Cf98ac6E23AaEF79B52717846'
}

// web3 state
let networkName
let currentNetwork
let accounts
let account
let tokenBalance

// instances
let controllerInstance
let tokenInstance
let clientInstance

// APPLICATION
const App = {
  start: function () {
    const self = this

    let controllerAddress
    let tokenAddress

    currentNetwork = web3.version.network
    if (currentNetwork === mainnet.id) {
      networkName = mainnet.name
      controllerAddress = mainnet.controller
      tokenAddress = mainnet.tokenAddress
      etherscanURL = mainnet.url
    } else if (currentNetwork === ropsten.id) {
      networkName = ropsten.name
      controllerAddress = ropsten.controller
      tokenAddress = ropsten.token
      etherscanURL = ropsten.url
    } else if (currentNetwork === null) {
      alert('Seems you have have not connection with web3, please reopen your Metamask/Mist.')
      return
    } else {
      alert('Please switch to the Ropsten network for testing')
      return
    }

    controllerInstance = Controller.at(controllerAddress)
    tokenInstance = IERC20.at(tokenAddress)

    self.runSemanticUiStaff()

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function (err, accs) {
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
        return
      }

      accounts = accs
      account = accounts[0]
      web3.eth.defaultAccount = account

      tokenInstance.balanceOf(account, (err, res) => {
        if (!err) {
          tokenBalance = parseInt(self.removeDecimalPart(res))
          $('#balance').text(`Your Balance - ${tokenBalance} ST`)
          $('#balance').attr('href', `${etherscanURL}/token/${tokenAddress}`)

          $('#contollerAddress').text(`dApp smart contract - ${controllerAddress}`)
          $('#contollerAddress').attr('href', `${etherscanURL}/address/${controllerAddress}`)

          $('#myaccount').attr('href', `${etherscanURL}/address/${account}`)

          self.updateAccountDetails().then(() => {
            self.initTransactions()
          })
        }
      })
    })
  },

  updateAccountDetails: function () {
    let r = document.getElementById('accountdetails').rows

    return new Promise((resolve, reject) => {
      controllerInstance.getUserContractAddress.call(account, (err, address) => {
        if (!err) {
          if (address === '0x0000000000000000000000000000000000000000') {
            r[1].cells[0].innerHTML = 'The account unexistent (create a schedule call for start)'
            r[1].cells[1].innerHTML = '-'
            r[1].cells[2].innerHTML = '_'
            r[1].cells[3].innerHTML = '_'
            r[1].cells[4].innerHTML = networkName
            $('[id=erc20transfer]')[0].disabled = true
          } else {
            if (address.length === 42) {
              clientInstance = ScheduleClient.at(address)
              Promise.all([this.getId(), this.getTokenAmount(address), this.getETHBalance(address)]).then((values) => {
                r[1].cells[0].innerHTML = `<a href="${etherscanURL}/address/${address}" target="_blank">${address}<a/>`
                r[1].cells[1].innerHTML = values[0]
                r[1].cells[2].innerHTML = this.removeDecimalPart(values[1]) + ' ST'
                r[1].cells[3].innerHTML = this.removeDecimalPart(values[2]) + ' ETH'
                r[1].cells[4].innerHTML = networkName
                $('[id=erc20transfer]')[0].disabled = false
                resolve()
              })
            }
          }
        } else reject(err)
      })
    })
  },

  // -----------------------------------------
  // TRANSACTIONS GETTERS
  // -----------------------------------------

  initTransactions: function () {
    let table = document.getElementById('activeTxs')
    let row = table.insertRow(1)
    let cell1 = row.insertCell(0)

    cell1.innerHTML = '<div class="ui inline active loader"></div> Pending'
    row.innerHTML = '<div id="loader"><div class="ui inline active loader"></div> Loading Blockchain Data</div>'

    this.getId().then(id => {
      let dataHash = []
      let active = []
      let canceled = []
      let executed = []

      for (let i = 0; i < id; i++) {
        this.getHash(i).then((hash) => {
          dataHash.push(hash)
          if (dataHash.length === id) {
            for (let l = 0; l < dataHash.length; l++) {
              this.getHashInfo(dataHash[l]).then((data) => {
                if (data.canceled === true) {
                  canceled.push(data)
                } else if (data.executionsAmount === data.executed) {
                  executed.push(data)
                } else {
                  active.push(data)
                }

                // After fetcih finish show transactions
                if (active.length + canceled.length + executed.length === dataHash.length) {
                  this.showTransactions(active, canceled, executed)
                }
              })
            }
          }
        })
      }
    })
  },

  showTransactions: function (active, canceled, executed) {
    $('#activeTxs tbody tr').remove()
    let table = document.getElementById('activeTxs').getElementsByTagName('tbody')[0]

    // Executed
    for (let i = 0; i < executed.length; i++) {
      let row = table.insertRow(0)

      row.setAttribute('id', executed[i].hash)
      row.innerHTML =
      `<td><a href="${etherscanURL}/address/${executed[i].receiver}"
      target="_blank">${executed[i].receiver}</td>
      <td>Executed</td>
      <td>${executed[i].delay}</td>
      <td>${executed[i].tokensPerDelay} ST</td>
      <td>${executed[i].executed} / ${executed[i].executionsAmount}</td>
      <td>${executed[i].creationDate}</td>
      <td><button class="ui compact mini red button" type="submit"
      onclick="App.cancelTx('${executed[i].hash}')" disabled>Cancel</button></td>`
    }

    // Cancelled
    for (let i = 0; i < canceled.length; i++) {
      let row = table.insertRow(0)

      row.setAttribute('id', canceled[i].hash)
      row.innerHTML =
      `<td><a href="${etherscanURL}/address/${canceled[i].receiver}"
      target="_blank">${canceled[i].receiver}</td>
      <td>Canceled</td>
      <td>${canceled[i].delay}</td>
      <td>${canceled[i].tokensPerDelay} ST</td>
      <td>${canceled[i].executed} / ${canceled[i].executionsAmount}</td>
      <td>${canceled[i].creationDate}</td>
      <td><button class="ui compact mini red button" type="submit"
      onclick="App.cancelTx('${canceled[i].hash}')" disabled>Cancel</button></td>`
    }

    // scheduled
    for (let i = 0; i < active.length; i++) {
      let row = table.insertRow(0)

      row.setAttribute('id', active[i].hash)
      row.innerHTML =
      `<td><a href="${etherscanURL}/address/${active[i].receiver}"
      target="_blank">${active[i].receiver}</td>
      <td>Active</td>
      <td>${active[i].delay}</td>
      <td>${active[i].tokensPerDelay} ST</td>
      <td>${active[i].executed} / ${active[i].executionsAmount}</td>
      <td>${active[i].creationDate}</td>
      <td><button class="ui compact mini red button" type="submit"
      onclick="App.cancelTx('${active[i].hash}')">Cancel</button></td>`
    }

    $('#activeTxs #loader').remove()
  },

  runSemanticUiStaff: function () {
    const self = this
    $('.ui.dropdown').dropdown()
    $('#addSchedule').form({
      fields: {
        receiver: {
          identifier: 'receiver',
          rules: [
            {
              type   : 'exactLength[42]',
              prompt : 'Please enter valid receiver address (42 symbols)'
            }
          ]
        },
        tokens: {
          identifier: 'tokens',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter valid tokens amount'
            }
          ]
        },
        delay: {
          identifier: 'delay',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter valid delay'
            }
          ]
        },
        executions: {
          identifier: 'executions',
          rules: [
            {
              type   : 'empty',
              prompt : 'Please enter valid executions amount (should be bigger then 1)'
            }
          ]
        }
      },
      onSuccess: function (e) {
        e.preventDefault()
        self.addScheduleTransaction()
      }
    })
    $('#transferTokens').form({
      fields: {
        token_amount: {
          identifier: 'token_amount',
          rules: [
            {
              type   : 'empty',
              prompt : 'The token amount will be calculated automatically related to schedule calls'
            }
          ]
        }
      },
      onSuccess: function (e) {
        e.preventDefault()
        let tokenAmount = $('[name=token_amount]').val()
        self.transferTokens(tokenAmount)
      }
    })
  },

  // -----------------------------------------
  // GETTERS FROM CLIENT CONTRACT
  // -----------------------------------------

  getId: function () {
    return new Promise((resolve, reject) => {
      clientInstance.getId.call((err, value) => {
        if (!err) return resolve(parseInt(value))
        else reject(err)
      })
    })
  },

  getHash: function (id) {
    return new Promise((resolve, reject) => {
      clientInstance.schedules.call(id, (err, value) => {
        if (!err) return resolve(value)
        else reject(err)
      })
    })
  },

  getHashInfo: function (hash) {
    return new Promise((resolve, reject) => {
      clientInstance.getScheduleDetails(hash, (err, data) => {
        if (!err) {
          let dataObj = {
            receiver: data[0],
            delay: parseInt(data[1] / 60) + ' min',
            executed: parseInt(data[2]),
            executionsAmount: parseInt(data[3]),
            tokensPerDelay: this.removeDecimalPart(data[4]),
            canceled: data[5],
            creationDate: this.unixToNormalDate(data[6]),
            hash: hash.toString()
          }
          return resolve(dataObj)
        } else reject(err)
      })
    })
  },

  getTokenAmount: function (address) {
    return new Promise((resolve, reject) => {
      tokenInstance.balanceOf.call(web3.toChecksumAddress(address), (err, value) => {
        if (!err) return resolve(value)
        else reject(err)
      })
    })
  },

  getETHBalance: function (address) {
    return new Promise((resolve, reject) => {
      web3.eth.getBalance(web3.toChecksumAddress(address), (err, value) => {
        if (!err) return resolve(value)
        else reject(err)
      })
    })
  },

  // -----------------------------------------
  // SETTERS FROM CLIENT CONTRACT
  // -----------------------------------------

  transferTokens: function (amount) {
    if (!this.zeroBalanceValidation()) {
      return
    }

    amount = this.toBigNumber(amount)

    tokenInstance.transfer(clientInstance.address, amount,
      { 'from': account, 'gasPrice': gasPrice }, (err, res) => {
        if (err) {
          this.showErrorTx()
        } else {
          this.showPendingTx(res)
          this.clearInputs()
          this.followTransaction(res).then(() => {
            console.log('tokens transfered')
          })
        }
      })
  },

  callScheduleCalls: function (to, executionsUNIX, executions, tokens) {
    let executionPrice = executions * gasLimit * gasPrice

    return new Promise((resolve, reject) => {
      controllerInstance.scheduleCall(to, executionsUNIX, executions, tokens,
        { 'from': account, 'value': executionPrice },
        (err, res) => {
          if (err) {
            this.showErrorTx()
            reject(err)
          } else {
            this.showPendingTx(res)
            this.clearInputs()
            this.followTransaction(res).then(() => {
              console.log('new schedule call created')
              resolve()
            })
          }
        })
    })
  },

  addScheduleTransaction: function () {
    if (!this.zeroBalanceValidation()) {
      return
    }

    let to = web3.toChecksumAddress($('[name=receiver]').val())
    let tokens = parseInt(this.toBigNumber($('[name=tokens]').val()))
    let interval = $('[name=interval]').val()
    let delay = $('[name=delay]').val()
    let executions = parseInt($('[name=executions]').val())
    let tokensAmount = parseInt($('[name=tokens]').val()) * executions
    let executionsUNIX

    switch (interval) {
      case 'min':
        executionsUNIX = delay * 60
        break
      case 'hour':
        executionsUNIX = delay * 60 * 60
        break
      case 'day':
        executionsUNIX = delay * 60 * 60 * 24
        break
      case 'year':
        executionsUNIX = delay * 60 * 60 * 24 * 30
        break
    }

    let isFirstSchedule = typeof clientInstance === 'undefined'

    if (isFirstSchedule) {
      this.callScheduleCalls(to, executionsUNIX, executions, tokens).then(() => {
        this.transferTokens(tokensAmount)
      })
    } else {
      this.transferTokens(tokensAmount)
      this.callScheduleCalls(to, executionsUNIX, executions, tokens)
    }
  },

  cancelTx: function (hash) {
    this.getHashInfo(hash).then(data => {
      let createdAtUnix = (new Date(data.creationDate).getTime() / 1000)
      let delay = parseInt(data.delay) * 60
      let executed = data.executed
      let executionsAmount = data.executionsAmount

      web3.eth.getBlock('latest', (err, block) => {
        if (!err) {
          let now = block.timestamp
          if (
            (
              now < createdAtUnix + ((executed + 1) * delay) - 120 ||
              now > createdAtUnix + ((executed + 1) * delay) + 120
            ) &&
            (
              executionsAmount > executed
            )
          ) {
            clientInstance.cancel(hash, (err, res) => {
              if (err) {
                this.showErrorTx()
              } else {
                this.showPendingTx(res)
                this.followTransaction(res).then(() => {
                  console.log('schedule canceled')
                })
              }
            })
          } else if (executionsAmount === executed) {
            alert('The schedule was finished')
          } else {
            alert('Due security reasons You cannot cancel schedule call +- 2 minutes from next scheduled call')
          }
        }
      })
    })
  },

  emergencyExit: function () {
    confirm(`
      ATTENTION

      With this method you will disable all schedule calls,
      all invested ETH and tokens will be returned back to your.

      Are you serious want to do that?
    `)
    clientInstance.emergencyExit((err, res) => {
      if (err) {
        this.showErrorTx()
      } else {
        this.showPendingTx(res)
        this.followTransaction(res).then(() => {
          console.log('emergency exit used')
        })
      }
    })
  },

  // -----------------------------------------
  // HELPERS
  // -----------------------------------------

  toBigNumber: function (amount) {
    const decimals = web3.toBigNumber(18)
    const bigNumber = web3.toBigNumber(amount)
    const value = bigNumber.times(web3.toBigNumber(10).pow(decimals))

    return value
  },

  removeDecimalPart: function (amount) {
    return amount.div(Math.pow(10, 18))
  },

  showPendingTx: function (res) {
    let table = document.getElementById('submitedTxs')
    table.style.display = 'table'
    let row = table.insertRow(1)
    let cell1 = row.insertCell(0)
    let cell2 = row.insertCell(1)
    row.setAttribute('id', res)
    cell1.innerHTML = `<a href="${etherscanURL}/tx/${res}" target="_blank">${res}<a/>`
    cell2.innerHTML = '<div class="ui inline active loader"></div> Pending'
  },

  clearInputs: function () {
    $('[name=token_amount]').val('')
    $('[name=receiver]').val('')
    $('[name=tokens]').val('')
    $('[name=delay]').val('')
    $('[name=executions]').val('')
  },

  showErrorTx: function () {
    alert('Error schediling transaction. Operation was cancelled')
  },

  followTransaction: function (tx) {
    return new Promise((resolve, reject) => {
      this.getTransactionReceiptMined(tx).then(() => {
        // update view
        this.updateAccountDetails().then(() => {
          // hiding existing tx
          $(`#${tx}`).remove()

          // init new tx
          this.initTransactions()
          resolve()
        })
      })
    })
  },

  getTransactionReceiptMined: function (txHash) {
    return new Promise((resolve, reject) => {
      (function transactionReceiptAsync () {
        web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
          if (error) {
            reject(error)
          } else if (receipt == null) {
            setTimeout(
              () => transactionReceiptAsync()
              , 5000)
          } else {
            console.log('block mined!')
            resolve(receipt)
          }
        })
      })()
    })
  },

  unixToNormalDate: function (unix) {
    // Months array
    const monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    // Convert unix to milliseconds
    const date = new Date(unix * 1000)

    // Year
    const year = date.getFullYear()

    // Month
    const month = monthsArr[date.getMonth()]

    // Day
    const day = date.getDate()

    // Hours
    const hours = date.getHours()

    // Minutes
    const minutes = '0' + date.getMinutes()

    // Seconds
    const seconds = '0' + date.getSeconds()

    return month + '-' + day + '-' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2)
  },

  zeroBalanceValidation: function () {
    if (tokenBalance === 0) {
      alert('You have not ST tokens, please purchase it for using this dApp')
      return false
    } else {
      return true
    }
  }
}

window.App = App

window.addEventListener('load', async () => {
  if (window.ethereum) {
    window.web3 = new Web3(window.ethereum)
    try {
      await window.ethereum.enable()
    } catch (error) {
      alert('Please allow the Metamask to using our platform')
    }
  } else if (window.web3) {
    window.web3 = new Web3(web3.currentProvider)
  } else {
    console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
  }

  App.start()
})
